﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    // Use this for initialization
    public void Play()
    {
        SceneManager.LoadScene("Cinematica");
    }

    public void Controles()
    {
        SceneManager.LoadScene("Controles");
    }

    public void EndGame()
    {

        Application.Quit();
    }

}