﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Victory : MonoBehaviour {

    private MouseCursor mouseCursor;

    public void Start()
    {

        mouseCursor = new MouseCursor();
    }

    public void Update()
    {
        mouseCursor.ShowCursor();
    }

    // Use this for initialization
    public void Play()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void EndGame()
    {

        Application.Quit();
    }

}
