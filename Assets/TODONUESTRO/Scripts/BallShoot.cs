﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallShoot : MonoBehaviour
{
    public LayerMask mask;

    public GameObject bullet_prefab;
    float bulletImpulse = 20f;

    public float hitForce;
    public float hitDamage;
    public int granadas = 5;
    public AudioClip[] fx;
    private AudioSource audioSource;
    public Text numeroGranadas;
    public int Cgranadas;
    //Debug
    private Vector3 impactPosition;
    public ChangeGun cambioarma;

    public void Awake()
    {
        numeroGranadas.text = "" + Cgranadas;
    }

    // Update is called once per frame
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void Shot()
    {

        if (cambioarma.armas2 == true)
        {

            if (granadas > 0)
            {


                Cgranadas--;
                granadas--;
                numeroGranadas.text = "" + Cgranadas;

                GameObject thebullet = (GameObject)Instantiate(bullet_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
                thebullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);

                PlaySFX(0, 0.25f, true);

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Debug.DrawRay(this.transform.position, ray.direction, Color.red, 10);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask, QueryTriggerInteraction.Collide))
                {
                    if (hit.rigidbody != null)
                    {
                        hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
                    }
                    Enemy enemy = hit.transform.GetComponent<Enemy>();
                    if (enemy != null)
                    {
                        enemy.TakeDamage(hitDamage);
                    }


                    Enemy_sargento enemys = hit.transform.GetComponent<Enemy_sargento>();
                    if (enemys != null)
                    {
                        enemys.TakeDamage(hitDamage);
                    }
                    hit.transform.gameObject.SendMessage("Damage", 1);

                    impactPosition = hit.point;

                    GameObject impact = new GameObject("Shot Impact");
                    impact.transform.position = impactPosition;
                    AudioSource audioImpact = impact.AddComponent<AudioSource>();
                    audioImpact.clip = fx[1];
                    audioImpact.volume = 0.15f;
                    audioImpact.Play();
                    Destroy(impact, fx[1].length);
                }

            }
        }
    }

    void PlaySFX(int clip, float volume, bool randomPitch)
    {
        if (fx.Length < clip || clip < 0)
        {
            Debug.LogError("Clip does not exist: " + clip);
            return;
        }

        audioSource.clip = fx[clip];
        audioSource.volume = volume;

        if (randomPitch)
        {
            audioSource.pitch = Random.Range(0.95f, 1.05f);
        }

        audioSource.Play();
    }
}
