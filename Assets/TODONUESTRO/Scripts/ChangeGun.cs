﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGun : MonoBehaviour {

    public GameObject arma1;
    public GameObject arma2;
    public GameObject arma3;

    public bool armas1;
    public bool armas2;
    public bool armas3;

    private void Start()
    {
        arma1.SetActive(true);
        arma2.SetActive(false);
        arma3.SetActive(false);
        armas1 = true;
        armas2 = false;
        armas3 = false;
    }


    void Update()
    {
        if ((armas1 == false || armas2 == false) && (Input.GetKey(KeyCode.Alpha1)))
        {
            arma1.SetActive(true);
            arma2.SetActive(false);
            arma3.SetActive(false);
            armas1 = true;
            armas2 = false;
            armas3 = false;
        }
        if ((armas3 == false || armas2 == false) &&  (Input.GetKey(KeyCode.Alpha2)))
        {
            arma1.SetActive(false);
            arma2.SetActive(true);
            arma3.SetActive(false);
            armas1 = false;
            armas2 = true;
            armas3 = false;
        }
        if ((armas1 == false || armas3 == false) && (Input.GetKey(KeyCode.Alpha3)))
        {
            arma1.SetActive(false);
            arma2.SetActive(false);
            arma3.SetActive(true);
            armas1 = false;
            armas2 = false;
            armas3 = true;
        }

    }

    
 
}
