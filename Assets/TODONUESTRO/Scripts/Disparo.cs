﻿using UnityEngine;
using System.Collections;

public class Disparo : MonoBehaviour {

    public Transform origin;
    public float cadenciaDisparoPorSegundo = 1f;
    public int capacidadCargador = 6;
    public float alcance = 50f;
    public float danyo = 5f;
    public int serieBala = 1;
    public LayerMask equipoDanyable;
    public int numeroDanyados = 2;
    public GameObject animacion;
    public int animaciondisparo = 1;


    private int serie, cargador;
    private float tiempo;
    public AudioSource disparo;
    //private bool disparoPulsado = false;



    void Start()
    {
        this.serie = 0;
        this.cargador = this.capacidadCargador;
        this.tiempo = Time.time;
    }
	
	// Update is called once per frame
	void Update () {

        //BI pulsado. Disparo
        if (Input.GetMouseButton(0))
        {
            disparo.Play();
            animacion.GetComponent<Animation>().Play("Disparar",PlayMode.StopAll);

            if (this.cargador > 0)
            {
                if (Time.time - this.tiempo >= this.cadenciaDisparoPorSegundo)
                {
                    this.tiempo = Time.time;
                    this.cargador--;
                    //Dañar a Objetivos
                    Ray ray = new Ray(this.origin.position, this.origin.forward);
                    RaycastHit[] hits = Physics.RaycastAll(ray, this.alcance);
                    for (int i = 0; i < Mathf.Min(hits.Length, this.numeroDanyados); i++)
                    {
                        if (this.equipoDanyable == (this.equipoDanyable | (1 << hits[i].transform.gameObject.layer)))
                        {
                            Debug.Log(hits[i].transform.name + " - Daño: " + this.danyo.ToString());
                            
                        }
                        else break;
                    }
                    //Instanciar Bala
                    if (this.serie % this.serieBala == 0)
                    {
                        Debug.Log("Bala Instanciada: " + Time.time.ToString());
                    }
                    this.serie++;
                }
            }
            else Debug.Log("Cargador Vacio");
        }
        //Recargar
        if (Input.GetKeyDown(KeyCode.R))
        {
            this.cargador = this.capacidadCargador;
            Debug.Log("Arma Recargada");
        }	
	}
}
